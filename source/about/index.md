---
title: about
date: 2017-07-21 11:36:55
---

Mon nom est un private joke qui me fait sourire et qui correspond bien à l'idée que je me fais de cet espace. Où suis-je et où t'emmené-je ?
Dans une partie de mon univers je suppose. Au risque de perdre du monde je vais mélanger toutes sortes de pensées, allant de la sphère professionnelle à celle plus personnelle. Puisqu'au final c'est l'ensemble de ces éléments qui font ce que je suis.
Ceci est donc mon espace d'écriture numérique dans le but d'un petit partage. Il a commencé par {% post_link hello-world ici %}. 
