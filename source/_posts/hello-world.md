---
title: Hello World !
date: 2017-07-21 14:00:29
tags: [dans ma tête]
---

 J'ai entendu un élève s'exclamer "Mais qu'est qu'ils ont tous en informatique avec Hello world!". Cela m'a fait sourire, Hello world pour moi c'est juste le souvenir de mes années d'études, une pincée de nostalgie.
 Je relis "Autoportrait de l'auteur en coureur de fond" de Haruki Murakami. Il commence par nous indiquer qu'il n'est pas là pour dire ce qu'il faut faire ni comment le faire. C'est juste un journal sur un thème précis. C'est la même pensée que Richard Templar dans "S'organiser sans se fouler". Je suis vraiment en accord avec cela. Cet espace est un partage d'expériences ni plus ni moins.   
