#+TITLE: Plouf !
#+TAGS: dans une piscine, dans l'eau
#+DATE: <2018-11-25>

Je vais vous faire une petite révélation, je ne sais pas nager. Genre
pas du tout. Quand je le dis, les gens comprennent que je nage mais pas
très bien. Comme si je disais que je ne sais pas courir. Alors que
non, il faut le prendre comme quelqu'un qui dit qu'il ne sait pas
faire de vélo. Alors si tu es comme moi, bienvenue ici :-) Accroche
toi au mur ou à la bouée je vais te raconter mon aventure. Si au
contraire cela te paraît presque irréaliste, reste ici je vais te
montrer que tout est possible :-) 

En effet voici une deuxième révélation, cela est bientôt de l'histoire
ancienne :-D. Depuis quelques semaines je prends des cours, à
raison d'un par semaine plus une séance libre. Je progresse petits pas
par petits pas.

Lors de ma dernière séance j'ai effectué un grand plouf dans les
profondeurs de la piscine. J'ai expérimenté que mon corps flotte et
remonte tout seul à la surface, et c'est très agréable :-).

Je vais tâcher de revenir régulièrement ici pour te raconter cette
grande aventure. Mes premières séances, le pourquoi du comment et en
quel animal aquatique je me transforme !
