---
title: "Comment j'ai apprivoisé Twitter !"
date: 2017-11-27 10:35:56
tags: [sur le net]
---

Hello Twitter, bonjour les pubs et les avis de publications. Voici pour moi les deux maux de Twitter. Ils bousculent complètement le flux faisant perdre des publications. Le temps n'est pas respecté, et c'est ainsi que je râle régulièrement en me disant qu'un jour je vais fuir. En attendant de naviguer vers Mastodon j'ai cherché un remède tel que Twitdeck. Aussi je n'aime pas trop ses multiples colonnes qui complexifient ma lecture. J'ai donc trouvé une autre solution qui me convient bien en créant des listes. Oui parce qu'on peut créer des listes dans Twitter !
En effet celles-ci ne sont pas, pour le moment, polluées par des pubs ni une modification du flux. J'ai ainsi trois listes : une pour les consultations quotidiennes, une autre pour les mensuelles et une dernière pour les temporaires qui concerne des événements fixés dans le temps. Et pour simplifier l'accès à chacune j'ai créé dans mon navigateur web des marque-pages. Cette astuce à également l'avantage d'alléger le flux. Les nouvelles que j'estime pouvant attendre quelques jours avant d'être périmées sont classées dans la liste mensuelle. Je pourrais également faire une liste hebdomadaire à consulter le vendredi par exemple, ou encore loisirs à consulter le week-end. Cela me permet une utilisation plus zen, plus raisonnée, plus structurée. Les tweets ne se perdent plus dans le flux continu. Le seul inconvénient est sa mise en place, trier plus de 100 comptes est vite laborieux ! Et pour cela j'ai utilisé [Twitlistmanager](http://twitlistmanager.com/) en triant un peu tout les jours. J'en ai ainsi profité pour faire le tri dans les comptes que je suis.

Petit point sur Mastodon, c'est la perte d'information dans ma veille me fait aujourd'hui hésiter, n'ayant pas envie de multiplier les outils. Mais c'est certainement une affaire à suivre...
