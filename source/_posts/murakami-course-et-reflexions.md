---
title: "Murakami, course et réflexions"
date: 2017-08-03 13:24:59
tags: [coin lecture]
---

Je termine ma relecture du livre "Autoportrait de l'auteur en coureur de fond" de Haruki Murakami. C'est une lecture rapide très enrichissante avec une réflexion sur des moments de course à pied. Certains livres nous apportent une part de rêve ici on nous partage des parts de doutes. Chaque coureur a certainement eu droit à LA question, "pourquoi tu cours ?", et c'est probablement ce qui fait que nous nous la posons. Je me suis donc déjà questionnée, me demandant pourquoi, alors que j'ai un métier plutôt solitaire, je pratique un sport à dominance solitaire. Finalement c'est un peu la même chose que pour un écrivain. J'ai envie de dire que le travail ne laisse pas la place aux pensées vagabondes. Qu'un travail de concentration assis sur sa chaise demande une bonne condition physique et mentale. Finalement si j'aime mon métier et mon sport c'est qu'ils me correspondent, introvertie que je suis.

Les lectures sont des expériences qui nous font prendre conscience d'une multitude de choses, nous faisant grandir. J'ai lu il y a quelques temps plusieurs articles[^1]  du genre "Je suis introvertie et je vous emmerde". Moi qui me battait avec l'étiquette d'être timide, cette étiquette collée par d'autres et qui gratte. D'un seul coup j'ai pu m'en débarrasser pour de bon. Je sais ce que je suis et j'aime cette nouvelle étiquette pleine de liberté.


[^1]:
* (http://www.huffingtonpost.fr/2013/08/22/23-signes-que-vous-etes-secretement-introverti_n_3791846.html)
* (http://www.huffingtonpost.fr/2014/11/15/je-suis-introverti-10-facons-interagissent-differemment-monde_n_6156970.html)
* (http://www.madmoizelle.com/introvertie-pas-malheureuse-395525)
* (http://www.madmoizelle.com/etre-introvertie-temoignage-259749)
