---
title: rtfm
date: 2018-06-14 09:37:01
tags: [la tête dans le code, au boulot]
---

RTFM ou comment perdre une journée !

Voilà un petit moment que je suis sur cette erreur. Relisons la doc, au cas où ! Il y a bien ce petit encart mais je ne sais pas pourquoi depuis le début je me dit qu'il ne me concerne pas ! En désespoir je vais quand même essayer. Et là, ça marche, je ne sais pas si je dois me réjouir d'avoir réussi ou m'en vouloir de ne pas avoir commencé par là. Fais ce que je dis pas ce que je fais. C'est comme s'énerver de ne pas réussir à monter un meuble Ikea alors qu'on n'a pas lu la notice. Ceci est ma première erreur mais au cours de cette journée il y en a eu une autre, ne pas lire le message d'erreur. Faire l'autruche. Je lance mon programme et regarde le résultat à l'écran. Au lieu de me concentrer sur l'erreur qu'affiche le programme je me concentre sur celle qui est sur l'écran. Et je fais ce qu'il ne faut pas faire. Ce que j'essaie d'expliquer à mes étudiants. Je fais une recherche sur le net de ladite erreur. Soit comment perdre un temps fou. Pourtant la première est plus explicite et même plus logique. C'est d'ailleurs lorsque je me décide de vraiment la prendre en compte que je reprends la doc. Je me demande vraiment pourquoi j'ai cette capacité à faire l'autruche, je mets cela du même ordre que la procrastination, je me sabote moi même ! Puisse ce post me permettre de ne plus faire ces erreurs stupides !
