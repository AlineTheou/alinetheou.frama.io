#+TITLE: Lecture : Sylvain Tesson et Mike Horn
#+DATE: <2019-05-26 Sun>
#+TAGS: coin lecture


Retour sur mes deux premières lectures de 2019 où je me
suis évadée grâce aux récits de deux aventuriers. Je suis partie avec
Sylvain Tesson une demie année dans une petite cabane au fin fond
de la Russie, puis avec Mike Horn sur les hauts sommets du Pakistan.

file:lecture-1-2019/livres-aventuriers.jpg


Certains livres viennent à moi ! J'aime me promener dans une librairie,
me laisser attirer par une couverture, un titre. Retourner le livre
pour y lire la 4ème page de couverture qui conduira à un achat
impulsif. C'est le cas de ce premier roman. Je suis envoûtée par la
couverture bleue, ses tranches bleues et les pages collées par cette
couleur. L'auteur part six mois en solitaire dans une petite cabane. Cela fait écho en moi avec ce qui se passe à ce moment là
dans ma vie. Il est certain que quelques jours auparavant je n'y aurais
pas prêté plus attention. Et quelques jours plus tard non plus ! 
L'auteur nous livre jour après jour les quelques notes de son
aventure. Je me sens capable de me lancer dans cette petite folie que
de quitter le confort dans lequel je suis pour apprécier le silence,
la nature et écouter mon monologue intérieur.  

Puis je suis partie avec Mike Horn. Entre deux sommets il nous raconte
des bouts de son enfance et de son passage à l'âge adulte. C'est une
lecture très intéressante pour comprendre le personnage. On prend bien consicence à travers son récit que l'on peut tous avoir
des rêves et les réaliser mais que nos bagages sont des aides précieuses. Bien que l'on peut s'upgrader toute une vie on part tous
avec une répartition de points de compétences bien à nous !

Les deux livres comportent à peu près le même nombre de pages mais
j'ai eu plus de facilité de lecture pour le deuxième. Je ne peux que
vous les conseillers si vous avez envie d'aventures, et je n'ai plus
qu'à partir moi-même dans ma propre conquête :)


