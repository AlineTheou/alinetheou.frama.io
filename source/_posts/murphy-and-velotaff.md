---
title: "Loi de Murphy en vélotaff !"
date: 2018-05-17 14:01:01
tags: [sur mon vélo]
---

Cela fait plusieurs jours que je me le répète, il faut que je regonfle mon vélo avant de subir une crevaison. Ce qui devait arriver arriva, j'ai regonflé et j'ai crevé ! La crevaison perverse. Celle qui te permet d'arriver à destination sans encombre. Qui se dégonfle lentement de sorte que c'est le soir, en reprenant ton vélo, que tu te rends compte du problème. Petit tour de la situation, c'est la roue arrière avec moyeux Nexus. Donc une réparation plus embêtante. Pour preuve tu paies plus cher pour ce type de roue qu'une normale. Bien entendu pour que l'histoire soit drôle c'est le jour où je n'ai pas ma carte bleue, donc une réparation sur l'instant n'est pas possible. Évidement je n'ai pas non plus ni ma carte de tram ni le prix d'un ticket en petite monnaie :-( Heureusement j'ai quand même un collègue sympa :-)
Bref, le lendemain je cherche une solution et souhaite en profiter pour changer le pneu qui a déjà eu une belle vie. C'est bientôt l'été, il fait beau et tout le monde roule à vélo. Ce qui fait que les réparateurs n'ont pas trop le temps pour des interventions minutes. Aussi on me conseille d'aller au café vélo "Le maquis", et là petit bonheur l'affaire est conclue pour le soir même :-)

Les leçons de cette histoire :
- ne pas oublier de transférer sa carte de tram et sa carte bancaire dans ses affaires du boulot
- j'ai pris connaissance de plusieurs adresses pas trop loin de mon travail me permettant de réparer mon vélo
- les horaires de tram où tu peux voyager avec ton fidèle compagnon qui ne roule plus : avant 7h, entre 9h et 17h, et après 19h (sauf dimanche et jour férié)

Bon pédalage !

{% instagram Bi3875pgerR %}
