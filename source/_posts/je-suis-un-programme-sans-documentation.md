---
title: Je suis un programme non documenté !
date: 2017-07-25 13:19:36
tags: [dans ma tête]
---

Un des exercices courus en cours d'informatique est de donner un bout de code et de demander d'expliquer son comportement. C'est un exercice qu'un informaticien expérimente plus ou moins régulièrement. Mais dans notre vie d'humain, là, tout les jours, c'est exactement la même chose. Je suis née sans documentation ! Le programme fonctionne plus ou moins bien et pour le décoder ce n'est pas toujours une mince affaire. Il faudra certainement des bonnes longueurs d'années afin de disposer d'une documentation exhaustive. C'est presque un travail du quotidien. J'ai éclairci quelques mystères mais la tâche est encore longue.
Ceci est le résultat de mes réflexions de ce midi est j'ai bien envie de m'écrire cette doc, parce-qu'on le sait tous on perd parfois le sens de ce qu'on a su décrypter une autre fois. Je crois que ça peut être assez marrant :-)

Mon idée, à l'état de germination donc, est d'écrire une petite doc sur comment je fonctionne à partir de mes observations. Par exemple ne pas dormir n'est pas top ! Comme je prépare un cours PHP pour mes futurs étudiants, je vais faire cette documentation à partir d'un code PHP simple. Je pense pour le moment à définir des fonctions, telles que manger(), dormir() sans les décrire et de générer la doc avec un générateur. Mes observations seront faites à l'aide d'issues sur mon Gitlab. Voilà c'est un projet complètement farfelu mais qui m'amuse beaucoup ! Je vais quand même parler du but qui est assez simple, comprendre comment mon programme fonctionne afin de l'utiliser de façon optimum.  
