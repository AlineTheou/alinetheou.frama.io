---
title: "Plugin me !"
date: 2017-08-23 13:29:12
tags: []
---

Un petit coup de scotch vite fait bien fait, parce-que sortir le
tournevis ou la perceuse c'est mieux mais j'ai la flemme. Mais faut
avouer le scotch bien moche ne tiens pas très bien et il faut s'en cesse le
remettre en place. Et puis c'est quoi cette flemme de ne pas faire un
travail propre ? Un petit peu de temps, un jour plein d'énergie et
voilà de quoi réparer cela. Le tout terminé c'est un sentiment de
satisfaction à se souvenir pour éviter cette procrastination. 
