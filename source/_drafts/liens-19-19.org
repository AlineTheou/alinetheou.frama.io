#+TITLE: Liens de la semaine 19 de 2019
#+TAGS: dans mes livres

Petit article du vendredi soir avec mes trouvailles du net de la
semaine. Il est temps de faire vivre cet espace et pour cela je
m'inspire des articles "En vrac du jeudi" de Tristan Nitot. J'aime
beaucoup cette idée de partager des liens sur un espace calme qui te
permet de te poser et d'effectuer ta veille sur des thèmes qui
t'interpellent. Cela permet de partager et d'inviter aux
découvertes. C'est également une petite sauvegarde ce qui se passe
dans mon monde à un instant donné.


Trois petits liens pour commencer.

*  
- https://www.arte.tv/fr/videos/073938-000-A/l-homme-a-mange-la-terre/

*
- https://www.toolinux.com/?Installer-Linux-sur-un-smartphone-ou-une-tablette-Samsung-avec-Linux-on-DeX
- https://www.linuxondex.com/

*
